import pandas as pd
def bq_to_parquet(query:str, file_name:str):
    today_date = pd.datetime.now().strftime("%y%m%d")
    df = pd.read_gbq(query)
    df.to_parquet(f"Data/{file_name}_{today_date}.parquet")